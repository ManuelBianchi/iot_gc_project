#include "MeasurementPropagation.h"


//similar to valuePropagation

Define_Module(MeasurementPropagation);

void MeasurementPropagation::startup()
{
        //tempThreshold = par("tempThreshold"); //This is the tempThreshold
        //totalPackets = 0;
        //currMaxReceivedValue = -1.0;
        //currMaxSensedValue = -1.0;
        //sentOnce = 0;
        //theValue = 0;
        //setTimer(REQUEST_SAMPLE, 0);

        packet_rate = par("packet_rate");
        recipientAddress = par("nextRecipient").stringValue();
        recipientId = atoi(recipientAddress.c_str());
        startupDelay = par("startupDelay");
        delayLimit = par("delayLimit");
        packet_spacing = packet_rate > 0 ? 1 / float (packet_rate) : -1;
        dataSN = 0;

        numNodes = getParentModule()->getParentModule()->par("numNodes");
        packetsSent.clear();
        packetsReceived.clear();
        bytesReceived.clear();


        if (packet_spacing > 0 && recipientAddress.compare(SELF_NETWORK_ADDRESS) != 0)
                setTimer(SEND_PACKET, packet_spacing + startupDelay);
        else
                trace() << "Not sending packets";

        declareOutput("Packets received per node");

}

void MeasurementPropagation::timerFiredCallback(int index)
{

       //I use throughputTest

       switch (index) {
                case SEND_PACKET:{
                        trace() << "Sending packet #" << dataSN;
                        toNetworkLayer(createGenericDataPacket(0, dataSN), recipientAddress.c_str());
                        packetsSent[recipientId]++;
                        dataSN++;
                        setTimer(SEND_PACKET, packet_spacing);
                        break;
                }
        }

        //valuePropagation
        /*switch (index) {
                case REQUEST_SAMPLE:{
                        requestSensorReading();
                        break;
                }
        }
        //throughputTest
        switch (index) {
                case SEND_PACKET:{
                        trace() << "Sending packet #" << dataSN;
                        toNetworkLayer(createGenericDataPacket(0, dataSN), recipientAddress.c_str());
                        packetsSent[recipientId]++;
                        dataSN++;
                        setTimer(SEND_PACKET, packet_spacing);
                        break;
                }
        }*/
}
//forse è da modificare perché mi sembra che qui al di sotto del delaylimit non riceve nulla.
void MeasurementPropagation::fromNetworkLayer(ApplicationPacket * rcvPacket, const char *source, double rssi, double lqi)
{       

        //my code
        
        int sequenceNumber = rcvPacket->getSequenceNumber(); //sequenceNumber
        double receivedData = rcvPacket->getData(); //here there is temperature's value
        int sourceId = atoi(source); //nodo sorgente
        // Here below there should be end-to-end latency
        if( recipientAddress.compare(SELF_NETWORK_ADDRESS) ==0)
        {
         if (delayLimit == 0 || (simTime() - rcvPacket->getCreationTime() <= delayLimit)
         {
            //qui voglio mettere la temperatura come informazione.
            //trace() << "Received packet #" << sequenceNumber << " from node " << source;
              trace() << "Received packet #" << sequenceNumber << " from node " << source << "Temperature: " << receivedData;
                        collectOutput("Packets received per node", sourceId);
                        packetsReceived[sourceId]++;
                        bytesReceived[sourceId] += rcvPacket->getByteLength();
         } else {
                        trace() << "Packet #" << sequenceNumber << " from node " << source <<
                                " exceeded delay limit of " << delayLimit << "s";
                }
        } else {
                ApplicationPacket* fwdPacket = rcvPacket->dup();
                // Reset the size of the packet, otherwise the app overhead will keep adding on
                fwdPacket->setByteLength(0);
                toNetworkLayer(fwdPacket, recipientAddress.c_str());
        }




        //part of valuePropagation
        /*double receivedData = rcvPacket->getData();

        totalPackets++;
        if (receivedData > currMaxReceivedValue)
                currMaxReceivedValue = receivedData;

        if (receivedData > tempThreshold && !sentOnce) {
                theValue = receivedData;
                toNetworkLayer(createGenericDataPacket(receivedData, 1), BROADCAST_NETWORK_ADDRESS);
                sentOnce = 1;
                trace() << "Got the value: " << theValue;
        }*/
      
        //part of throughputTest
        
        /*int sequenceNumber = rcvPacket->getSequenceNumber();
        int sourceId = atoi(source);

        // This node is the final recipient for the packet
        if (recipientAddress.compare(SELF_NETWORK_ADDRESS) == 0) {
                if (delayLimit == 0 || (simTime() - rcvPacket->getCreationTime()) <= delayLimit) {
                        trace() << "Received packet #" << sequenceNumber << " from node " << source;
                        collectOutput("Packets received per node", sourceId);
                        packetsReceived[sourceId]++;
                        bytesReceived[sourceId] += rcvPacket->getByteLength();
                } else {
                        trace() << "Packet #" << sequenceNumber << " from node " << source <<
                                " exceeded delay limit of " << delayLimit << "s";
                }
        // Packet has to be forwarded to the next hop recipient
        } else {
                ApplicationPacket* fwdPacket = rcvPacket->dup();
                // Reset the size of the packet, otherwise the app overhead will keep adding on
                fwdPacket->setByteLength(0);
                toNetworkLayer(fwdPacket, recipientAddress.c_str());
        }*/

}

//read the data by sensor and create the packets.
//In src/node/sensorManager/SensorManager.ned the sensorTypes = Temperature ;
void MeasurementPropagation::handleSensorReading(SensorReadingMessage * rcvReading)
{
        double sensedValue = rcvReading->getSensedValue();

        /*if (sensedValue > currMaxSensedValue)
                currMaxSensedValue = sensedValue;

        if (sensedValue > tempThreshold && !sentOnce) {
                theValue = sensedValue;
                toNetworkLayer(createGenericDataPacket(sensedValue, 1), BROADCAST_NETWORK_ADDRESS);
                sentOnce = 1;
        }*/
}

//Take it from throughoutTest , it should be optional because write only in trace.
// This method processes a received carrier sense interupt. Used only for demo purposes
// in some simulations. Feel free to comment out the trace command.
void MeasurementPropagation::handleRadioControlMessage(RadioControlMessage *radioMsg)
{
        switch (radioMsg->getRadioControlMessageKind()) {
                case CARRIER_SENSE_INTERRUPT:
                        trace() << "CS Interrupt received! current RSSI value is: " << radioModule->readRSSI();
                        break;
        }
}

//here we can calculated the Packet delivery ratio, this function runs to the end of the simulation.
void MeasurementPropagation::finishSpecific()
{       
        declareOutput("Packets reception rate");
        declareOutput("Packets loss rate");

        cTopology *topo;        // temp variable to access packets received by other nodes
        topo = new cTopology("topo");
        topo->extractByNedTypeName(cStringTokenizer("node.Node").asVector());

        long bytesDelivered = 0;
        for (int i = 0; i < numNodes; i++) {
                //da cambiare
                ThroughputTest *appModule = dynamic_cast<ThroughputTest*>
                        (topo->getNode(i)->getModule()->getSubmodule("Application"));
                if (appModule) {
                        int packetsSent = appModule->getPacketsSent(self);
                        if (packetsSent > 0) { // this node sent us some packets
                                float rate = (float)packetsReceived[i]/packetsSent;
                                //collectOutput("Packets reception rate", i, "total", rate);
                                //I copied the function calling above, because it is the delivery ratio!
                                collectOutput("Packets delivery ratio",i,"total", rate);
                                collectOutput("Packets loss rate", i, "total", 1-rate);
                        }

                        bytesDelivered += appModule->getBytesReceived(self);
                }
        }
        delete(topo);

        if (packet_rate > 0 && bytesDelivered > 0) {
                double energy = (resMgrModule->getSpentEnergy() * 1000000000)/(bytesDelivered * 8);     //in nanojoules/bit
                declareOutput("Energy nJ/bit");
                collectOutput("Energy nJ/bit","",energy);
        }
        //throuputTest
        /*declareOutput("Packets reception rate");
        declareOutput("Packets loss rate");

        cTopology *topo;        // temp variable to access packets received by other nodes
        topo = new cTopology("topo");
        topo->extractByNedTypeName(cStringTokenizer("node.Node").asVector());

        long bytesDelivered = 0;
        for (int i = 0; i < numNodes; i++) {
                ThroughputTest *appModule = dynamic_cast<ThroughputTest*>
                        (topo->getNode(i)->getModule()->getSubmodule("Application"));
                if (appModule) {
                        int packetsSent = appModule->getPacketsSent(self);
                        if (packetsSent > 0) { // this node sent us some packets
                                float rate = (float)packetsReceived[i]/packetsSent;
                                collectOutput("Packets reception rate", i, "total", rate);
                                collectOutput("Packets loss rate", i, "total", 1-rate);
                        }

                        bytesDelivered += appModule->getBytesReceived(self);
                }
        }
        delete(topo);

        if (packet_rate > 0 && bytesDelivered > 0) {
                double energy = (resMgrModule->getSpentEnergy() * 1000000000)/(bytesDelivered * 8);     //in nanojoules/bit
                declareOutput("Energy nJ/bit");
                collectOutput("Energy nJ/bit","",energy);
        }*/

        //valuePropagation
        /*declareOutput("got value");
        if (theValue > tempThreshold)
                collectOutput("got value", "yes/no", 1);
        else
                collectOutput("got value", "yes/no", 0);
        declareOutput("app packets received");
        collectOutput("app packets received", "", totalPackets);*/
}


