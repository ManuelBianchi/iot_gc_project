#ifndef _MEASUREMENTPROPAGATION_H_
#define _MEASUREMENTPROPAGATION_H_

#include "VirtualApplication.h"

using namespace std;

enum MeasurementPropagationTimers {
        REQUEST_SAMPLE = 1,
};

class MeasurementPropagation: public VirtualApplication {
 private:
        int totalPackets;
        double currMaxReceivedValue;
        double currMaxSensedValue;
        int sentOnce;
        double theValue;
        double tempThreshold;
        vector<double> sensedValues;

 protected:
        void startup();
        void finishSpecific();
        void fromNetworkLayer(ApplicationPacket *, const char *, double, double);
        void handleSensorReading(SensorReadingMessage *);
        void timerFiredCallback(int);
};

#endif                          // _MEASUMENTPROPAGATION_APPLICATIONMODULE_H_

